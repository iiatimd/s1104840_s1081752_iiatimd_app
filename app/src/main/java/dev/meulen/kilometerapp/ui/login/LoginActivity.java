package dev.meulen.kilometerapp.ui.login;

import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.ViewModelProvider;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import dagger.hilt.android.AndroidEntryPoint;
import dev.meulen.kilometerapp.R;
import dev.meulen.kilometerapp.ui.ritten.RittenOverzichtActivity;

@AndroidEntryPoint
public class LoginActivity extends AppCompatActivity {

    private LoginViewModel loginViewModel;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        // Initiate Activity
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        // Assign viewModel
        loginViewModel = new ViewModelProvider(this).get(LoginViewModel.class);

        // Add custom toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.app_toolbar);
        setSupportActionBar(toolbar);

        // Assign all fields to variables
        final EditText emailEditText = findViewById(R.id.email);
        final EditText passwordEditText = findViewById(R.id.password);

        final Button loginButton = findViewById(R.id.login);
        final Button registerButton = findViewById(R.id.register);

        final ProgressBar loadingProgressBar = findViewById(R.id.loading);

        // Trigger the creation of a LiveData object for the loginFormState, and assign an observer
        // to monitor the state of the LiveData object
        loginViewModel.getLoginFormState().observe(this, loginFormState -> {

            // Abort any action if there is no state yet
            if (loginFormState == null) {
                return;
            }

            // Enable the loginButton if all validation checks pass
            loginButton.setEnabled(loginFormState.isDataValid());

            // If there is an error, assign it to the corresponding editText
            if (loginFormState.getEmailError() != null) {
                emailEditText.setError(getString(loginFormState.getEmailError()));
            }
            if (loginFormState.getPasswordError() != null) {
                passwordEditText.setError(getString(loginFormState.getPasswordError()));
            }

        });

        // Trigger the creation of a LiveData object for loginResult, and assign an observer
        // to monitor the state of the LiveData object
        loginViewModel.getLoginResult().observe(this, loginResult -> {

            // Abort any action if there is no state yet
            if (loginResult == null) {
                return;
            }

            // When a result is in, disable the loading icon
            loadingProgressBar.setVisibility(View.GONE);

            // Show the error we got from our viewModel, and return the function
            if (loginResult.getError() != null) {
                Toast.makeText(getApplicationContext(), loginResult.getError(), Toast.LENGTH_LONG).show();
                return;
            }

            // Assign the action to take when a login is successful
            if (loginResult.getSuccess() != null) {
                // Start RittenOverzichtActivity
                startActivity(new Intent(this, RittenOverzichtActivity.class));
                // Assign transition to the previous intent
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
            }

            // Complete and destroy login activity once successful
            finish();
        });

        // Create a TextWatcher to automatically view error messages to the user
        TextWatcher afterTextChangedListener = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // ignore
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // ignore
            }

            @Override
            public void afterTextChanged(Editable s) {
                // Notify the loginViewModel of the latest changes in our form
                loginViewModel.loginDataChanged(
                        emailEditText.getText().toString(),
                        passwordEditText.getText().toString()
                );
            }
        };

        // Add the TextWatcher to both input fields
        emailEditText.addTextChangedListener(afterTextChangedListener);
        passwordEditText.addTextChangedListener(afterTextChangedListener);

        // Add an actionListener to the last editText to make the enter button on the keyboard initiate a login
        passwordEditText.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                loginViewModel.login(
                        emailEditText.getText().toString(),
                        passwordEditText.getText().toString()
                );
            }
            return false;
        });

        // onClickListener for loginButton
        loginButton.setOnClickListener(v -> {
            // Make the loading icon visible
            loadingProgressBar.setVisibility(View.VISIBLE);
            // Make the call to log the user in
            loginViewModel.login(
                    emailEditText.getText().toString(),
                    passwordEditText.getText().toString()
            );
        });

        // onClickListener for registerButton
        registerButton.setOnClickListener(v -> {
            // Start RegisterActivity
            startActivity(new Intent(this, RegisterActivity.class));
            // Assign transition
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
            // No finish() statement here to allow the user to go back to the login screen
        });
    }

}
