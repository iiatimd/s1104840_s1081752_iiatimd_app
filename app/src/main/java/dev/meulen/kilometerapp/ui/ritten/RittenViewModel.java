package dev.meulen.kilometerapp.ui.ritten;

import android.util.Log;

import androidx.hilt.lifecycle.ViewModelInject;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.inject.Inject;

import dev.meulen.kilometerapp.R;
import dev.meulen.kilometerapp.data.LoginRepository;
import dev.meulen.kilometerapp.data.Result;
import dev.meulen.kilometerapp.data.model.Rit;
import dev.meulen.kilometerapp.ui.toevoegenRit.ToevoegenRitFormState;

public class RittenViewModel extends ViewModel {

    private RittenRepository rittenRepository;
    private LoginRepository loginRepository;

    private MutableLiveData<ToevoegenRitFormState> toevoegenRitFormState = new MutableLiveData<>();

    @Inject
    @ViewModelInject
    public RittenViewModel(RittenRepository rittenRepository, LoginRepository loginRepository) {
        this.rittenRepository = rittenRepository;
        this.loginRepository = loginRepository;
    }

    LiveData<List<Rit>> getRittenList() {
        return rittenRepository.getRittenFromUser(loginRepository.getLoggedInUser().getId());
    }

    LiveData<Result> refreshRittenList() {
        return rittenRepository.refreshAllRitten();
    }

    public LiveData<ToevoegenRitFormState> getToevoegenRitFormState() {
        return toevoegenRitFormState;
    }

    public void insert(Rit rit) {
        rit.setUserName(loginRepository.getLoggedInUser().getName());
        rit.setUserId(loginRepository.getLoggedInUser().getId());
        rittenRepository.insert(rit);
    }

    public void toevoegenRitDataChanged(String kmStart, String kmEind, String kenteken, String ritStart, String ritEind) {
        if (!isKmStartValid(kmStart)) {
            toevoegenRitFormState.setValue(new ToevoegenRitFormState(R.string.invalid_km_start, null, null, null));
        } else if (!isKmEindValid(kmStart, kmEind)) {
            toevoegenRitFormState.setValue(new ToevoegenRitFormState(null, R.string.invalid_km_eind, null, null));
        } else if (!isKentekenValid(kenteken)) {
            toevoegenRitFormState.setValue(new ToevoegenRitFormState(null, null, R.string.invalid_kenteken, null));
        } else if (!isDateValid(ritStart, ritEind)) {
            toevoegenRitFormState.setValue(new ToevoegenRitFormState(null, null, null, R.string.invalid_date));
        } else if (!isDateFutureValid(ritEind)) {
            toevoegenRitFormState.setValue(new ToevoegenRitFormState(null, null, null, R.string.invalid_date_future));
        } else {
            toevoegenRitFormState.setValue(new ToevoegenRitFormState(true));
        }
    }

    // kmStart validation check
    private boolean isKmStartValid(String kmStart) {
        if(kmStart != null) {
            try {
                return Integer.parseInt(kmStart) > 0;
            } catch(NumberFormatException ignored) {}
        }
        return false;
    }

    // kmEind validation check
    private boolean isKmEindValid(String kmStart, String kmEind) {
        if(kmStart != null && kmEind != null) {
            try {
                return Integer.parseInt(kmEind) > Integer.parseInt(kmStart);
            } catch(NumberFormatException ignored) {}
        }
        return false;
    }

    // kenteken validation check
    private boolean isKentekenValid(String kenteken) {
        if(kenteken != null) {
            ArrayList<Pattern> patterns = new ArrayList<>();
            // patterns aan de hand van de eigenschappen van een kenteken aan de hand van rdw( Rijksdienst voor het Wegverkeer)
            patterns.add(Pattern.compile("[A-Z]{2}-\\d{2}-\\d{2}"));        //  1: XX-99-99
            patterns.add(Pattern.compile("\\d{2}-\\d{2}-[A-Z]{2}"));        //  2: 99-99-XX
            patterns.add(Pattern.compile("\\d{2}-[A-Z]{2}-\\d{2}"));        //  3: 99-XX-99
            patterns.add(Pattern.compile("[A-Z]{2}-\\d{2}-[A-Z]{2}"));      //  4: XX-99-XX
            patterns.add(Pattern.compile("[A-Z]{2}-[A-Z]{2}-\\d{2}"));      //  5: XX-XX-99
            patterns.add(Pattern.compile("\\d{2}-[A-Z]{2}-[A-Z]{2}"));      //  6: 99-XX-XX
            patterns.add(Pattern.compile("\\d{2}-[A-Z]{3}-\\d"));           //  7: 99-XXX-9
            patterns.add(Pattern.compile("\\d-[A-Z]{3}-\\d{2}"));           //  8: 9-XXX-99
            patterns.add(Pattern.compile("[A-Z]{2}-\\d{3}-[A-Z]"));         //  9: XX-999-X
            patterns.add(Pattern.compile("[A-Z]-\\d{3}-[A-Z]{2}"));         // 10: X-999-XX
            patterns.add(Pattern.compile("[A-Z]{3}-\\d{2}-[A-Z]"));         // 11: XXX-99-X
            patterns.add(Pattern.compile("[A-Z]-\\d{2}-[A-Z]{3}"));         // 12: X-99-XXX
            patterns.add(Pattern.compile("\\d-[A-Z]{2}-\\d{3}"));           // 13: 9-XX-999
            patterns.add(Pattern.compile("\\d{3}-[A-Z]{2}-\\d"));           // 14: 999-XX-9
            patterns.add(Pattern.compile("CD[ABFJNST][0-9]{1,3}"));         // Diplomatieke platen (CDB1, CDJ45)

            // Loop through kenteken patterns
            for(Pattern pattern : patterns) {
                Matcher matcher = pattern.matcher(kenteken.toUpperCase());
                if(matcher.find()){
                    return true;
                }
            }
        }
        return false;
    }

    // date validation check
    private boolean isDateValid(String ritStart, String ritEind) {
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy - HH:mm");
            Date ritStartDate = simpleDateFormat.parse(ritStart);
            Date ritEindDate = simpleDateFormat.parse(ritEind);
            Log.d("Appje", "ritStart: " + ritStartDate + " ritEind: " + ritEindDate);
            return ritEind != null && ritEindDate.compareTo(ritStartDate) >= 0;
        } catch (ParseException e) {
            e.printStackTrace();
            return false;
        }
    }

    // date future validation check
    private boolean isDateFutureValid(String ritEind) {
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy - HH:mm");
            Date ritEindDate = simpleDateFormat.parse(ritEind);
            return ritEind != null && !ritEindDate.after(Calendar.getInstance().getTime());
        } catch (ParseException e) {
            e.printStackTrace();
            return false;
        }
    }

}
