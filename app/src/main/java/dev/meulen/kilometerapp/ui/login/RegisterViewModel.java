package dev.meulen.kilometerapp.ui.login;

import android.util.Log;
import android.util.Patterns;

import androidx.hilt.lifecycle.ViewModelInject;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import javax.inject.Inject;

import dev.meulen.kilometerapp.R;
import dev.meulen.kilometerapp.data.LoginRepository;
import dev.meulen.kilometerapp.data.Result;
import dev.meulen.kilometerapp.data.model.User;

public class RegisterViewModel extends ViewModel {

    private MutableLiveData<RegisterFormState> registerFormState = new MutableLiveData<>();
    private MutableLiveData<LoginResult> loginResult = new MutableLiveData<>();

    LoginRepository loginRepository;

    @Inject
    @ViewModelInject
    RegisterViewModel(LoginRepository loginRepository) {
        this.loginRepository = loginRepository;
    }

    LiveData<RegisterFormState> getRegisterFormState() {
        return registerFormState;
    }

    LiveData<LoginResult> getLoginResult() {
        return loginResult;
    }

    public void register(String username, String email, String password, String password_confirmation) {
        Executor executor = Executors.newSingleThreadExecutor();
        executor.execute(() -> {
            // can be launched in a separate asynchronous job
            Result<User> result = loginRepository.register(username, email, password, password_confirmation);

            if (result instanceof Result.Success) {
                @SuppressWarnings("unchecked")
                User data = ((Result.Success<User>) result).getData();
                loginResult.postValue(new LoginResult(new LoggedInUserView(data.getName())));
            } else {
                loginResult.postValue(new LoginResult(((Result.Error) result).getError().toString()));
            }
        });
    }

    public void registerDataChanged(String username, String email, String password, String password_confirmation) {
        if (!isUserNameValid(username)) {
            registerFormState.setValue(new RegisterFormState(R.string.invalid_username, null, null, null));
        } else if (!isEmailValid(email)) {
            registerFormState.setValue(new RegisterFormState(null, R.string.invalid_email, null, null));
        } else if (!isPasswordValid(password)) {
            registerFormState.setValue(new RegisterFormState(null, null, R.string.invalid_password, null));
        } else if (!isPasswordConfirmationValid(password, password_confirmation)) {
            registerFormState.setValue(new RegisterFormState(null, null, null, R.string.invalid_password_confirmation));
        } else {
            registerFormState.setValue(new RegisterFormState(true));
        }
    }

    // A placeholder username validation check
    private boolean isUserNameValid(String username) {
        if (username == null) {
            return false;
        } else {
            return !username.trim().isEmpty();
        }
    }

    // A placeholder email validation check
    private boolean isEmailValid(String email) {
        if (email == null) {
            return false;
        } else {
            return Patterns.EMAIL_ADDRESS.matcher(email).matches();
        }
    }

    // A placeholder password validation check
    private boolean isPasswordValid(String password) {
        return password != null && password.trim().length() > 5;
    }

    // A placeholder password confirmation check
    private boolean isPasswordConfirmationValid(String password, String password_confirmation) {
        return password.equals(password_confirmation);
    }
}
