package dev.meulen.kilometerapp.ui.toevoegenRit;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.ViewModelProvider;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import dagger.hilt.android.AndroidEntryPoint;

import dev.meulen.kilometerapp.R;
import dev.meulen.kilometerapp.ui.ritten.RittenOverzichtActivity;
import dev.meulen.kilometerapp.data.model.Rit;
import dev.meulen.kilometerapp.ui.ritten.RittenViewModel;

@AndroidEntryPoint
public class ToevoegenRitActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // Initiate Activity
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_toevoegen_rit);

        // Assign viewModel
        RittenViewModel rittenViewModel = new ViewModelProvider(this).get(RittenViewModel.class);

        // Add custom toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.app_toolbar);
        setSupportActionBar(toolbar);

        // Add back button to ActionBar
//        ActionBar actionBar = getSupportActionBar();
//        actionBar.setDisplayHomeAsUpEnabled(true);

        // Assign all fields to variables
        EditText beschrijvingEditText = findViewById(R.id.beschrijving);
        EditText kmStartEditText = findViewById(R.id.kmStart);
        EditText kmEindEditText = findViewById(R.id.kmEind);
        EditText kentekenEditText = findViewById(R.id.kenteken);
        EditText ritStartEditText = findViewById(R.id.ritStart);
        EditText ritEindEditText = findViewById(R.id.ritEind);

        Button opslaanButton = findViewById(R.id.opslaan);

        // Add inputFilter to kentekenEditText to force upper case input and limit length to 8 characters
        InputFilter[] newFilters = new InputFilter[3];
        newFilters[0] = new InputFilter.AllCaps();
        newFilters[1] = new InputFilter.LengthFilter(8);
        newFilters[2] = new InputFilter() {
            @Override
            public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
                boolean keepOriginal = true;
                StringBuilder sb = new StringBuilder(end - start);
                for (int i = start; i < end; i++) {
                    char c = source.charAt(i);
                    if (isCharAllowed(c)) // put your condition here
                        sb.append(c);
                    else
                        keepOriginal = false;
                }
                if (keepOriginal)
                    return null;
                else {
                    if (source instanceof Spanned) {
                        SpannableString sp = new SpannableString(sb);
                        TextUtils.copySpansFrom((Spanned) source, start, sb.length(), null, sp, 0);
                        return sp;
                    } else {
                        return sb;
                    }
                }
            }

            private boolean isCharAllowed(char c) {
                return Character.isLetterOrDigit(c) || c == '-';
            }
        };
        kentekenEditText.setFilters(newFilters);

        TextWatcher kentekenWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                try {
                    if(start + count > 1 && count > before) {
                        if (
                                Character.isDigit(s.charAt(start + before - 1)) && Character.isAlphabetic(s.charAt(start + count - 1)) ||
                                Character.isAlphabetic(s.charAt(start + before - 1)) && Character.isDigit(s.charAt(start + count - 1))
                        ) {
                            kentekenEditText.removeTextChangedListener(this);
                            String nieuw = s.subSequence(0, start + count - 1) + "-" + s.charAt(start + count - 1);
                            kentekenEditText.setText(nieuw);
                            kentekenEditText.setSelection(nieuw.length());
                            kentekenEditText.addTextChangedListener(this);
                        }
                    }
                    if(start + count > 3 && count > before) {
                        if(
                                Character.isAlphabetic(s.charAt(start + before - 1)) && Character.isAlphabetic(s.charAt(start + count - 1)) &&
                                Character.isAlphabetic(s.charAt(start + before - 2)) && Character.isAlphabetic(s.charAt(start + before - 3)) ||
                                Character.isDigit(s.charAt(start + before - 1)) && Character.isDigit(s.charAt(start + count - 1)) &&
                                Character.isDigit(s.charAt(start + before - 2)) && Character.isDigit(s.charAt(start + before - 3))
                        )
                        {
                            kentekenEditText.removeTextChangedListener(this);
                            String nieuw = s.subSequence(0, start + count - 2) + "-" + s.charAt(start + before - 1) + s.charAt(start + count - 1);
                            kentekenEditText.setText(nieuw);
                            kentekenEditText.setSelection(nieuw.length());
                            kentekenEditText.addTextChangedListener(this);
                        }
                    }
                } catch (IndexOutOfBoundsException e) {
                    e.printStackTrace();

                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        };

        kentekenEditText.addTextChangedListener(kentekenWatcher);

        // Initiate datePickers with current date
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy - HH:mm");
        ritStartEditText.setText(simpleDateFormat.format(Calendar.getInstance().getTime()));
        ritEindEditText.setText(simpleDateFormat.format(Calendar.getInstance().getTime()));

        // Set onClickListener for ritStartEditText
        ritStartEditText.setOnClickListener(v -> {
            Calendar calendar = Calendar.getInstance();
            try {
                calendar.setTime(simpleDateFormat.parse(ritStartEditText.getText().toString()));
            } catch (ParseException e) {
                e.printStackTrace();
            }

            // Create listener for ritStart DatePicker
            DatePickerDialog.OnDateSetListener ritStartDateSetListener = (viewDate, year, month, dayOfMonth) -> {
                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                calendar.set(Calendar.MONTH, month);
                calendar.set(Calendar.YEAR, year);

                TimePickerDialog.OnTimeSetListener ritStartTimeSetListener = (viewTime, hourOfDay, minute) -> {
                    calendar.set(Calendar.HOUR_OF_DAY,hourOfDay);
                    calendar.set(Calendar.MINUTE,minute);

                    ritStartEditText.setText(simpleDateFormat.format(calendar.getTime()));
                };

                new TimePickerDialog(
                        ToevoegenRitActivity.this,
                        ritStartTimeSetListener,
                        calendar.get(Calendar.HOUR_OF_DAY),
                        calendar.get(Calendar.MINUTE),
                        true)
                        .show();
            };

            new DatePickerDialog(
                    ToevoegenRitActivity.this,
                    ritStartDateSetListener,
                    calendar.get(Calendar.YEAR),
                    calendar.get(Calendar.MONTH),
                    calendar.get(Calendar.DAY_OF_MONTH))
                    .show();
        });

        // Set onClickListener for ritEindEditText
        ritEindEditText.setOnClickListener(v -> {
            Calendar calendar = Calendar.getInstance();
            try {
                calendar.setTime(simpleDateFormat.parse(ritEindEditText.getText().toString()));
            } catch (ParseException e) {
                e.printStackTrace();
            }

            // Create listener for ritEind DatePicker
            DatePickerDialog.OnDateSetListener ritEindDateSetListener = (viewDate, year, month, dayOfMonth) -> {
                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                calendar.set(Calendar.MONTH, month);
                calendar.set(Calendar.YEAR, year);

                TimePickerDialog.OnTimeSetListener ritEindTimeSetListener = (viewTime, hourOfDay, minute) -> {
                    calendar.set(Calendar.HOUR_OF_DAY,hourOfDay);
                    calendar.set(Calendar.MINUTE,minute);

                    ritEindEditText.setText(simpleDateFormat.format(calendar.getTime()));
                };

                new TimePickerDialog(
                        ToevoegenRitActivity.this,
                        ritEindTimeSetListener,
                        calendar.get(Calendar.HOUR_OF_DAY),
                        calendar.get(Calendar.MINUTE),
                        true)
                        .show();
            };

            new DatePickerDialog(
                    ToevoegenRitActivity.this,
                    ritEindDateSetListener,
                    calendar.get(Calendar.YEAR),
                    calendar.get(Calendar.MONTH),
                    calendar.get(Calendar.DAY_OF_MONTH))
                    .show();
        });

        // Trigger the creation of a LiveData object for the toevoegenRitFormState, and assign an observer
        // to monitor the state of the LiveData object
        rittenViewModel.getToevoegenRitFormState().observe(this, toevoegenRitFormState -> {

            // Abort any action if there is no state yet
            if (toevoegenRitFormState == null) {
                return;
            }

            // Enable the loginButton if all validation checks pass
            opslaanButton.setEnabled(toevoegenRitFormState.isDataValid());

            // If there is an error, assign it to the corresponding editText
            if (toevoegenRitFormState.getKmStartError() != null) {
                kmStartEditText.setError(getString(toevoegenRitFormState.getKmStartError()));
            }
            if (toevoegenRitFormState.getKmEindError() != null) {
                kmEindEditText.setError(getString(toevoegenRitFormState.getKmEindError()));
            }
            if (toevoegenRitFormState.getKentekenError() != null) {
                kentekenEditText.setError(getString(toevoegenRitFormState.getKentekenError()));
            }
            if (toevoegenRitFormState.getDateError() != null) {
                ritEindEditText.setError(getString(toevoegenRitFormState.getDateError()));
                Toast.makeText(getApplicationContext(), toevoegenRitFormState.getDateError(), Toast.LENGTH_LONG).show();
            } else {
                ritEindEditText.setError(null);
            }

        });

        // Create a TextWatcher to automatically view error messages to the user
        TextWatcher afterTextChangedListener = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // ignore
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // ignore
            }

            @Override
            public void afterTextChanged(Editable s) {
                // Notify the rittenViewModel of the latest changes in our form
                rittenViewModel.toevoegenRitDataChanged(
                        kmStartEditText.getText().toString(),
                        kmEindEditText.getText().toString(),
                        kentekenEditText.getText().toString(),
                        ritStartEditText.getText().toString(),
                        ritEindEditText.getText().toString()
                );
            }
        };

        // Add the TextWatcher to all input fields
        kmStartEditText.addTextChangedListener(afterTextChangedListener);
        kmEindEditText.addTextChangedListener(afterTextChangedListener);
        kentekenEditText.addTextChangedListener(afterTextChangedListener);
        ritStartEditText.addTextChangedListener(afterTextChangedListener);
        ritEindEditText.addTextChangedListener(afterTextChangedListener);

        // Set onClickListener for opslaanButton
        opslaanButton.setOnClickListener(v -> {
            try {
                rittenViewModel.insert(new Rit(
                        null, null, null,
                        beschrijvingEditText.getText().toString(),
                        simpleDateFormat.parse(ritStartEditText.getText().toString()),
                        simpleDateFormat.parse(ritEindEditText.getText().toString()),
                        Integer.valueOf(kmStartEditText.getText().toString()),
                        Integer.valueOf(kmEindEditText.getText().toString()),
                        kentekenEditText.getText().toString()
                ));
            } catch (ParseException e) {
                e.printStackTrace();
            }

            startActivity(new Intent(ToevoegenRitActivity.this, RittenOverzichtActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
            finish();
        });

    }

    @Override
    public void finish() {
        super.finish();
        //animatie teruggaan naar vorig scherm
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

}

