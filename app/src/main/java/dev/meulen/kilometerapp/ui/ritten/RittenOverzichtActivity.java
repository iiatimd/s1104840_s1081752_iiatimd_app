package dev.meulen.kilometerapp.ui.ritten;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import javax.inject.Inject;

import dagger.hilt.android.AndroidEntryPoint;
import dev.meulen.kilometerapp.MainActivity;
import dev.meulen.kilometerapp.R;
import dev.meulen.kilometerapp.data.LoginRepository;
import dev.meulen.kilometerapp.data.Result;
import dev.meulen.kilometerapp.ui.toevoegenRit.ToevoegenRitActivity;

@AndroidEntryPoint
public class RittenOverzichtActivity extends AppCompatActivity {

    // Make hilt inject the currently active loginRepository
    @Inject LoginRepository loginRepository;

    @Override
    @SuppressWarnings("unchecked")
    protected void onCreate(Bundle savedInstanceState) {
        // Initiate Activity
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ritten_overzicht);

        // Add custom toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.app_toolbar);
        setSupportActionBar(toolbar);

        // Setup RecyclerView
        RecyclerView recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.hasFixedSize();
        RittenAdapter rittenAdapter = new RittenAdapter(getApplicationContext());
        recyclerView.setAdapter(rittenAdapter);

        // Assign viewModel and Observer
        RittenViewModel rittenViewModel = new ViewModelProvider(this).get(RittenViewModel.class);
        rittenViewModel.getRittenList().observe(this, newRitten -> {
            rittenAdapter.setRitten(newRitten);
        });

        // Assign function to toevoegenRitButton
        FloatingActionButton toevoegenRitButton = findViewById(R.id.addRitButton);
        toevoegenRitButton.setOnClickListener(v -> {
            startActivity(new Intent(RittenOverzichtActivity.this, ToevoegenRitActivity.class));
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        });

        // Add swipeRefreshLayout with listener to add the refresh gesture
        final SwipeRefreshLayout pullToRefresh = findViewById(R.id.swipeRefreshLayout);
        pullToRefresh.setOnRefreshListener(() -> {
            rittenViewModel.refreshRittenList().observe(RittenOverzichtActivity.this, result -> {
                pullToRefresh.setRefreshing(false);
                if(result instanceof Result.Error) {
                    Toast.makeText(this, ((Result.Error) result).getError().toString(), Toast.LENGTH_LONG).show();
                }
            });
        });
    }

    // Inflate menu in custom ActionBar
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.action_main, menu);
        return true;
    }

    // Loop through all menu items on ActionBar
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_logout:
                // Call logout on loginRepository (takes care of the api and SharedPreferences)
                loginRepository.logout();

                // Start the MainActivity (which should result in opening loginActivity if the
                // logout call to loginRepository went correctly)
                startActivity(new Intent(this, MainActivity.class));

                // Add slide transition
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);

                // Destroy this activity, resulting in the back button returning to the home screen
                finish();

                return true;

            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }

}