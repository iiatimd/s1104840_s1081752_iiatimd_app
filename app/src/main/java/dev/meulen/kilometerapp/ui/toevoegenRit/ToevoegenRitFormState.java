package dev.meulen.kilometerapp.ui.toevoegenRit;

import androidx.annotation.Nullable;

public class ToevoegenRitFormState {

    @Nullable
    private Integer kmStartError;
    @Nullable
    private Integer kmEindError;
    @Nullable
    private Integer kentekenError;
    @Nullable
    private Integer dateError;
    private boolean isDataValid;

    public ToevoegenRitFormState(@Nullable Integer kmStartError, @Nullable Integer kmEindError, @Nullable Integer kentekenError, @Nullable Integer dateError) {
        this.kmStartError = kmStartError;
        this.kmEindError = kmEindError;
        this.kentekenError = kentekenError;
        this.dateError = dateError;
        this.isDataValid = false;
    }

    public ToevoegenRitFormState(boolean isDataValid) {
        this.kmStartError = null;
        this.kmEindError = null;
        this.kentekenError = null;
        this.dateError = null;
        this.isDataValid = isDataValid;
    }

    @Nullable
    Integer getKmStartError() {
        return kmStartError;
    }

    @Nullable
    Integer getKmEindError() {
        return kmEindError;
    }

    @Nullable
    Integer getKentekenError() {
        return kentekenError;
    }

    @Nullable
    Integer getDateError() {
        return dateError;
    }

    boolean isDataValid() {
        return isDataValid;
    }

}
