package dev.meulen.kilometerapp.ui.login;

import androidx.annotation.Nullable;

/**
 * Data validation state of the register form.
 */
class RegisterFormState {
    @Nullable
    private Integer nameError;
    @Nullable
    private Integer emailError;
    @Nullable
    private Integer passwordError;
    @Nullable
    private Integer passwordConfirmationError;
    private boolean isDataValid;

    RegisterFormState(@Nullable Integer nameError, @Nullable Integer emailError, @Nullable Integer passwordError, @Nullable Integer passwordConfirmationError) {
        this.nameError = nameError;
        this.emailError = emailError;
        this.passwordError = passwordError;
        this.passwordConfirmationError = passwordConfirmationError;
        this.isDataValid = false;
    }

    RegisterFormState(boolean isDataValid) {
        this.nameError = null;
        this.emailError = null;
        this.passwordError = null;
        this.passwordConfirmationError = null;
        this.isDataValid = isDataValid;
    }

    @Nullable
    Integer getNameError() {
        return nameError;
    }

    @Nullable
    Integer getEmailError() {
        return emailError;
    }

    @Nullable
    Integer getPasswordError() {
        return passwordError;
    }

    @Nullable
    Integer getPasswordConfirmationError() {
        return passwordConfirmationError;
    }

    boolean isDataValid() {
        return isDataValid;
    }
}
