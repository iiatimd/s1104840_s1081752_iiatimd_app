package dev.meulen.kilometerapp.ui.ritten;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.text.SimpleDateFormat;
import java.util.List;

import dev.meulen.kilometerapp.R;
import dev.meulen.kilometerapp.data.model.Rit;

public class RittenAdapter extends RecyclerView.Adapter<RittenAdapter.RittenViewHolder> {

    // Initiate private variables, one List with Rit objects and the application context
    private List<Rit> ritten;
    private Context context;

    // Get application context from constructor
    public RittenAdapter(Context context) {
        this.context = context;
    }

    // Inflate ritten_card into RittenViewHolder
    @NonNull
    @Override
    public RittenViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.ritten_card, parent, false);
        return new RittenViewHolder(v);
    }

    // Assign values for each entry in the List
    @Override
    public void onBindViewHolder(@NonNull RittenViewHolder holder, int position) {

        // Set date and time patterns
        SimpleDateFormat datum = new SimpleDateFormat("dd/MM/yyyy");
        SimpleDateFormat tijd = new SimpleDateFormat("HH:mm");

        // Assign data if available
        if(ritten != null) {
            holder.textViewNaam.setText(context.getResources().getString(R.string.card_naam) + " " + ritten.get(position).getUserName());
            holder.textViewKenteken.setText(context.getResources().getString(R.string.card_kenteken) + " " + ritten.get(position).getKenteken());
            holder.textViewDatum.setText(context.getResources().getString(R.string.card_datum) + " " + datum.format(ritten.get(position).getRitStart()));
            holder.textViewDuur.setText(context.getResources().getString(R.string.card_duur) + " " + tijd.format(ritten.get(position).getRitStart()) + " - " + tijd.format(ritten.get(position).getRitEind()));
            holder.textViewKmStand.setText(context.getResources().getString(R.string.card_kmStand) + " " + String.valueOf(ritten.get(position).getKmEind()));
            holder.textViewAfstand.setText(context.getResources().getString(R.string.card_afstand) + " " + String.valueOf(ritten.get(position).getKmEind()-ritten.get(position).getKmStart()));
            String beschrijving = ritten.get(position).getBeschrijving();
            if(beschrijving != null) {
                holder.textViewBeschrijving.setText(context.getResources().getString(R.string.card_beschrijving) + " " + beschrijving);
            }
        }

    }

    // Sets data for ritten variable and notifies the RecyclerView
    public void setRitten(List<Rit> ritten) {
        this.ritten = ritten;
        notifyDataSetChanged();
    }

    // Return the size of the current data when available
    @Override
    public int getItemCount() {
        if(ritten != null) {
            return ritten.size();
        } else {
            return 0;
        }
    }

    // ViewHolder for RecyclerView
    public class RittenViewHolder extends RecyclerView.ViewHolder {

        // Initiate TextViews
        TextView textViewNaam, textViewKenteken, textViewDatum, textViewDuur, textViewKmStand, textViewAfstand, textViewBeschrijving;

        // Assign TextViews to Resource ID's
        public RittenViewHolder(View v){
            super(v);
            textViewNaam = v.findViewById(R.id.naam);
            textViewKenteken = v.findViewById(R.id.kenteken);
            textViewDatum = v.findViewById(R.id.datum);
            textViewDuur = v.findViewById(R.id.duur);
            textViewKmStand = v.findViewById(R.id.kmStand);
            textViewAfstand = v.findViewById(R.id.afstand);
            textViewBeschrijving = v.findViewById(R.id.beschrijving);
        }

    }
}
