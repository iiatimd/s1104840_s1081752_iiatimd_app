package dev.meulen.kilometerapp.ui.ritten;

import android.content.Context;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.work.BackoffPolicy;
import androidx.work.Constraints;
import androidx.work.Data;
import androidx.work.NetworkType;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;

import com.google.gson.Gson;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import dagger.hilt.android.qualifiers.ApplicationContext;
import dev.meulen.kilometerapp.data.Result;
import dev.meulen.kilometerapp.data.api.KilometerService;
import dev.meulen.kilometerapp.data.database.dao.RitDao;
import dev.meulen.kilometerapp.data.model.Rit;
import dev.meulen.kilometerapp.data.work.RitWorker;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RittenRepository {

    private RitDao ritDao;
    private ExecutorService dbExecutor;

    Context context;
    KilometerService service;
    Gson gson;

    @Inject
    RittenRepository(RitDao ritDao, ExecutorService databaseWriteExecutor, @ApplicationContext Context context, KilometerService service, Gson gson) {
        this.ritDao = ritDao;
        this.dbExecutor = databaseWriteExecutor;
        this.context = context;
        this.service = service;
        this.gson = gson;
    }

    LiveData<List<Rit>> getAllRitten() {
        refreshAllRitten();
        return ritDao.getAllRitten();
    }

    // Retrieve all relevant data for the current User
    LiveData<List<Rit>> getRittenFromUser(Integer userId) {
        refreshAllRitten();
        return ritDao.getRittenFromUser(userId);
    }

    @SuppressWarnings("unchecked")
    LiveData<Result> refreshAllRitten() {

        MutableLiveData<Result> result = new MutableLiveData<>();

        service.getAllRitten().enqueue(new Callback<List<Rit>>() {
            @Override
            public void onResponse(Call<List<Rit>> call, Response<List<Rit>> response) {
                dbExecutor.execute(() -> {
//                    ritDao.deleteAll();
                    ritDao.insertAll(response.body());
                });
                result.setValue(new Result.Success("Success"));
            }

            @Override
            public void onFailure(Call<List<Rit>> call, Throwable t) {
                result.setValue(new Result.Error(new IOException("Server onbereikbaar")));
            }
        });

        return result;
    }

    // Add rit to database, and trigger API request
    void insert(Rit rit) {
        dbExecutor.execute(() -> {
            ritDao.insert(rit);
        });

        Data data = new Data.Builder()
                .putString("rit", gson.toJson(rit))
                .build();

        Constraints constraints = new Constraints.Builder()
                .setRequiredNetworkType(NetworkType.CONNECTED)
                .build();

        OneTimeWorkRequest request = new OneTimeWorkRequest.Builder(RitWorker.class)
                .setInputData(data)
                .setConstraints(constraints)
                .addTag("rit")
                .setBackoffCriteria(BackoffPolicy.EXPONENTIAL, 30, TimeUnit.SECONDS)
                .build();

        WorkManager.getInstance(context)
                .enqueue(request);
    }
}
