package dev.meulen.kilometerapp.ui.login;

import androidx.hilt.lifecycle.ViewModelInject;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import android.util.Patterns;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import javax.inject.Inject;

import dev.meulen.kilometerapp.data.LoginRepository;
import dev.meulen.kilometerapp.data.Result;
import dev.meulen.kilometerapp.data.model.User;
import dev.meulen.kilometerapp.R;

public class LoginViewModel extends ViewModel {

    private MutableLiveData<LoginFormState> loginFormState = new MutableLiveData<>();
    private MutableLiveData<LoginResult> loginResult = new MutableLiveData<>();

    LoginRepository loginRepository;

    @Inject
    @ViewModelInject
    LoginViewModel(LoginRepository loginRepository) {
        this.loginRepository = loginRepository;
    }

    LiveData<LoginFormState> getLoginFormState() {
        return loginFormState;
    }

    LiveData<LoginResult> getLoginResult() {
        return loginResult;
    }

    public void login(String email, String password) {
        Executor executor = Executors.newSingleThreadExecutor();
        executor.execute(() -> {
            // can be launched in a separate asynchronous job
            Result<User> result = loginRepository.login(email, password);

            if (result instanceof Result.Success) {
                @SuppressWarnings("unchecked")
                User data = ((Result.Success<User>) result).getData();
                loginResult.postValue(new LoginResult(new LoggedInUserView(data.getName())));
            } else {
                loginResult.postValue(new LoginResult(((Result.Error) result).getError().toString()));
            }
        });
    }

    public void loginDataChanged(String email, String password) {
        if (!isEmailValid(email)) {
            loginFormState.setValue(new LoginFormState(R.string.invalid_email, null));
        } else if (!isPasswordValid(password)) {
            loginFormState.setValue(new LoginFormState(null, R.string.invalid_password));
        } else {
            loginFormState.setValue(new LoginFormState(true));
        }
    }

    // A placeholder email validation check
    private boolean isEmailValid(String email) {
        if (email == null) {
            return false;
        } else {
            return Patterns.EMAIL_ADDRESS.matcher(email).matches();
        }
    }

    // A placeholder password validation check
    private boolean isPasswordValid(String password) {
        return password != null && password.trim().length() > 5;
    }
}
