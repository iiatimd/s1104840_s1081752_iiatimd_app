package dev.meulen.kilometerapp.data.model;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

@Entity(/*foreignKeys = @ForeignKey(entity = User.class, parentColumns = "id", childColumns = "user_id", onDelete = CASCADE), */tableName = "ritten_table")
public class Rit {

    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "id")
    @SerializedName("id")
    @Expose
    private Integer id;

    @ColumnInfo(name = "user_id")
    @SerializedName("user_id")
    @Expose
    private Integer userId;

    @ColumnInfo(name = "user_name")
    @SerializedName("user_name")
    @Expose
    private String userName;

    @ColumnInfo(name = "rit_start")
    @SerializedName("rit_start")
    @Expose
    private Date ritStart;

    @ColumnInfo(name = "rit_eind")
    @SerializedName("rit_eind")
    @Expose
    private Date ritEind;

    @ColumnInfo(name = "km_start")
    @SerializedName("km_start")
    @Expose
    private Integer kmStart;

    @ColumnInfo(name = "km_eind")
    @SerializedName("km_eind")
    @Expose
    private Integer kmEind;

    @ColumnInfo(name = "kenteken")
    @SerializedName("kenteken")
    @Expose
    private String kenteken;

    @ColumnInfo(name = "beschrijving")
    @SerializedName("beschrijving")
    @Expose
    private String beschrijving;

    public Rit() {};

    @Ignore
    public Rit(@NonNull Integer id, Integer userId, String userName, String beschrijving, Date ritStart, Date ritEind, Integer kmStart, Integer kmEind, String kenteken) {
        this.id = id;
        this.userId = userId;
        this.userName = userName;
        this.ritStart = ritStart;
        this.ritEind = ritEind;
        this.kmStart = kmStart;
        this.kmEind = kmEind;
        this.kenteken = kenteken;
        this.beschrijving = beschrijving;
    }

    @NonNull
    public Integer getId() {
        return id;
    }

    public void setId(@NonNull Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Date getRitStart() {
        return ritStart;
    }

    public void setRitStart(Date ritStart) {
        this.ritStart = ritStart;
    }

    public Date getRitEind() {
        return ritEind;
    }

    public void setRitEind(Date ritEind) {
        this.ritEind = ritEind;
    }

    public Integer getKmStart() {
        return kmStart;
    }

    public void setKmStart(Integer kmStart) {
        this.kmStart = kmStart;
    }

    public Integer getKmEind() {
        return kmEind;
    }

    public void setKmEind(Integer kmEind) {
        this.kmEind = kmEind;
    }

    public String getKenteken() {
        return kenteken;
    }

    public void setKenteken(String kenteken) {
        this.kenteken = kenteken;
    }

    public String getBeschrijving() {
        return beschrijving;
    }

    public void setBeschrijving(String beschrijving) {
        this.beschrijving = beschrijving;
    }

    @Override
    public String toString() {
        return "id: " + id + " userId: " + userId + " userName: " + userName + " ritStart: " + ritStart + " ritEind: " + ritEind + " kmStart: " + kmStart + " kmEind: " + kmEind + " kenteken: " + kenteken + " beschrijving: " + beschrijving;
    }
}
