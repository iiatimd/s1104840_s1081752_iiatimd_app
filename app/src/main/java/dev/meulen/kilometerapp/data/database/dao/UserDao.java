package dev.meulen.kilometerapp.data.database.dao;

import androidx.room.Dao;
import androidx.room.Insert;

import dev.meulen.kilometerapp.data.model.User;

@Dao
public interface UserDao {

    @Insert
    void save(User user);

//    @Query("SELECT * FROM user WHERE userId = :userLogin")
//    LiveData<User> load(String userLogin);

}
