package dev.meulen.kilometerapp.data.api;

import java.util.List;

import dev.meulen.kilometerapp.data.model.APIToken;
import dev.meulen.kilometerapp.data.model.User;
import dev.meulen.kilometerapp.data.model.Rit;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface KilometerService {
    @FormUrlEncoded
    @POST("login")
    Call<APIToken> getAPIToken(@Field("email") String email, @Field("password") String password);

    @FormUrlEncoded
    @POST("register")
    Call<APIToken> register(@Field("name") String username, @Field("email") String email, @Field("password") String password, @Field("password_confirmation") String password_confirmation);

    @GET("user")
    Call<User> getLoggedInUser(@Header("Authorization") String token);
    
    @POST("rit")
    Call<Void> insertRit(@Body Rit rit);

    @GET("rit")
    Call<List<Rit>> getAllRitten();
}