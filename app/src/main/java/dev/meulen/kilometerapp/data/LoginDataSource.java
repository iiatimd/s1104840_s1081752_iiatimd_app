package dev.meulen.kilometerapp.data;

import android.util.Log;

import org.json.JSONObject;

import dev.meulen.kilometerapp.data.api.KilometerService;
import dev.meulen.kilometerapp.data.model.APIToken;
import dev.meulen.kilometerapp.data.model.User;
import retrofit2.Response;

import java.io.IOException;

import javax.inject.Inject;

/**
 * Class that handles authentication w/ login credentials and retrieves user information.
 */
public class LoginDataSource {

    KilometerService service;

    @Inject
    public LoginDataSource(KilometerService service) {
        this.service = service;
    }

    @SuppressWarnings("unchecked")
    public Result<User> login(String email, String password) {

        try {
            // Execute call synchronously
            Response loginResponse = service.getAPIToken(email, password).execute();

            if (loginResponse.isSuccessful()) {
                APIToken loginResult = (APIToken) loginResponse.body();

                Response loggedInUserResponse = service.getLoggedInUser(loginResult.getTokenType() + " " + loginResult.getAccessToken()).execute();

                if (loggedInUserResponse.isSuccessful()) {
                    User loggedInUser = (User) loggedInUserResponse.body();
                    return new Result.Success<>(loggedInUser);
                } else {
                    throw new IOException("Wrong access token");
                }

            } else {
                throw new IOException("Wrong credentials");
            }

        } catch (Exception e) {
            return new Result.Error(e);
        }
    }

    @SuppressWarnings("unchecked")
    public Result<User> register(String username, String email, String password, String password_confirmation) {

        try {
            // Execute call synchronously
            Response registerResponse = service.register(username, email, password, password_confirmation).execute();

            if (registerResponse.isSuccessful()) {
                APIToken registerResult = (APIToken) registerResponse.body();

                Response loggedInUserResponse = service.getLoggedInUser(registerResult.getTokenType() + " " + registerResult.getAccessToken()).execute();

                if (loggedInUserResponse.isSuccessful()) {
                    User loggedInUser = (User) loggedInUserResponse.body();
                    return new Result.Success<>(loggedInUser);
                } else {
                    try {
                        Log.d("Appje", loggedInUserResponse.errorBody().string());
                    } catch (Exception e) {
                        Log.d("Appje", e.getMessage());
                    }
                    throw new IOException("Wrong access token");
                }

            } else {
                String error;
                try {
                    error = registerResponse.errorBody().string();
                    JSONObject jObjError = new JSONObject(error);
                    if(jObjError.getJSONObject("error").has("email")) {
                        error = jObjError.getJSONObject("error").getJSONArray("email").getString(0);
                    } else {

                        error = jObjError.getJSONObject("error").toString();
                    }
                } catch (Exception e) {
                    error = e.getMessage();
                }
                throw new IOException(error);
            }

        } catch (Exception e) {
            return new Result.Error(e);
        }
    }

    public void logout() {
        // TODO: revoke authentication
    }
}
