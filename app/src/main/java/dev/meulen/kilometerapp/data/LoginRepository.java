package dev.meulen.kilometerapp.data;

import android.content.Context;
import android.content.SharedPreferences;

import javax.inject.Inject;
import javax.inject.Singleton;

import dagger.hilt.android.qualifiers.ApplicationContext;
import dev.meulen.kilometerapp.R;
import dev.meulen.kilometerapp.data.model.User;

/**
 * Class that requests authentication and user information from the remote data source and
 * maintains an in-memory cache of login status and user credentials information.
 */
@Singleton
public class LoginRepository {

    private LoginDataSource dataSource;
    private SharedPreferences sharedPreferences;
    private Context context;

    // If user credentials will be cached in local storage, it is recommended it be encrypted
    // @see https://developer.android.com/training/articles/keystore
    private User user = null;

    @Inject
    public LoginRepository(LoginDataSource dataSource, SharedPreferences sharedPreferences, @ApplicationContext Context context) {
        this.dataSource = dataSource;
        this.sharedPreferences = sharedPreferences;
        this.context = context;

        if(sharedPreferences.getInt(context.getResources().getString(R.string.preference_user_id), 0) != 0) {
            setLoggedInUser(new User(
                    sharedPreferences.getInt(context.getResources().getString(R.string.preference_user_id), 0),
                    sharedPreferences.getString(context.getResources().getString(R.string.preference_user_name), "")
            ));
        }

    }

    public boolean isLoggedIn() {
        return user != null;
    }

    public void logout() {
        user = null;
        dataSource.logout();
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.remove(context.getResources().getString(R.string.preference_user_id));
        editor.remove(context.getResources().getString(R.string.preference_user_name));
        editor.commit();
    }

    private void setLoggedInUser(User user) {
        this.user = user;
        // If user credentials will be cached in local storage, it is recommended it be encrypted
        // @see https://developer.android.com/training/articles/keystore
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(context.getResources().getString(R.string.preference_user_id), user.getId());
        editor.putString(context.getResources().getString(R.string.preference_user_name), user.getName());
        editor.commit();
    }

    public User getLoggedInUser() {
        return user;
    }

    @SuppressWarnings("unchecked")
    public Result<User> login(String email, String password) {
        // handle login
        Result<User> result = dataSource.login(email, password);
        if (result instanceof Result.Success) {
            setLoggedInUser(((Result.Success<User>) result).getData());
        }
        return result;
    }

    @SuppressWarnings("unchecked")
    public Result<User> register(String username, String email, String password, String password_confirmation) {
        // handle register
        Result<User> result = dataSource.register(username, email, password, password_confirmation);
        if (result instanceof Result.Success) {
            setLoggedInUser(((Result.Success<User>) result).getData());
        }
        return result;
    }
}
