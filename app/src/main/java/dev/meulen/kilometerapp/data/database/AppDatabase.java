package dev.meulen.kilometerapp.data.database;

import androidx.room.Database;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import javax.inject.Singleton;

import dev.meulen.kilometerapp.data.database.converter.DateConverter;
import dev.meulen.kilometerapp.data.database.dao.RitDao;
import dev.meulen.kilometerapp.data.database.dao.UserDao;
import dev.meulen.kilometerapp.data.model.APIToken;
import dev.meulen.kilometerapp.data.model.Rit;
import dev.meulen.kilometerapp.data.model.User;

@Singleton
@Database(entities = {User.class, Rit.class, APIToken.class}, version = 3, exportSchema = false)
@TypeConverters(DateConverter.class)
public abstract class AppDatabase extends RoomDatabase {
    public abstract UserDao userDao();
    public abstract RitDao ritDao();
}
