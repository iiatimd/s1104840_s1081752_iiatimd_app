package dev.meulen.kilometerapp.data.database.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

import dev.meulen.kilometerapp.data.model.Rit;

@Dao
public interface RitDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Rit rit);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<Rit> ritten);

    @Query("DELETE FROM ritten_table")
    void deleteAll();

    @Query("SELECT * FROM ritten_table WHERE id == :id")
    LiveData<Rit> getRit(Integer id);

    @Query("SELECT * FROM ritten_table WHERE user_id == :userId ORDER BY km_eind DESC")
    LiveData<List<Rit>> getRittenFromUser(Integer userId);

    @Query("SELECT * FROM ritten_table")
    LiveData<List<Rit>> getAllRitten();

}
