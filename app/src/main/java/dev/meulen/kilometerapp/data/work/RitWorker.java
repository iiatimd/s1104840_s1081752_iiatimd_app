package dev.meulen.kilometerapp.data.work;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.os.Build;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.hilt.Assisted;
import androidx.hilt.work.WorkerInject;
import androidx.work.Data;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import com.google.gson.Gson;

import dev.meulen.kilometerapp.R;
import dev.meulen.kilometerapp.data.LoginRepository;
import dev.meulen.kilometerapp.data.api.KilometerService;
import dev.meulen.kilometerapp.data.model.Rit;
import dev.meulen.kilometerapp.data.model.User;
import retrofit2.Response;

public class RitWorker extends Worker {

    public KilometerService service;

    public LoginRepository loginRepository;

    public Gson gson;

    //notification
    private static final String CHANNEL_ID= "KilometerApp";
    private static final String CHANNEL_NAME= "KilometerApp";
    private static final String CHANNEL_DECS= "KilometerApp Notifications";

    private Context context;

    @WorkerInject
    public RitWorker(@Assisted @NonNull Context context, @Assisted @NonNull WorkerParameters workerParams, KilometerService service, LoginRepository loginRepository, Gson gson) {
        super(context, workerParams);
        this.context = context;
        this.service = service;
        this.loginRepository = loginRepository;
        this.gson = gson;
    }

    @NonNull
    @Override
    public Result doWork() {
        try {
            Data data = getInputData();
            String jsonRit = data.getString("rit");
            Rit rit = gson.fromJson(jsonRit, Rit.class);
            User loggedInUser = loginRepository.getLoggedInUser();
            rit.setUserId(loggedInUser.getId());
            rit.setUserName(loggedInUser.getName());
            Log.d("appje", "worker triggered");
            Log.d("appje", rit.toString());
            Response response = service.insertRit(rit).execute();

            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
                NotificationChannel channel = new NotificationChannel(CHANNEL_ID, CHANNEL_NAME, NotificationManager.IMPORTANCE_DEFAULT);
                channel.setDescription(CHANNEL_DECS);
                NotificationManager manager = context.getSystemService(NotificationManager.class);
                manager.createNotificationChannel(channel);
            }

            if(response.isSuccessful()) {
                displayNotification("Rit opgeslagen", "De rit is aangemaakt en opgeslagen in de database!");
                return Result.success();
            } else {
                Log.d("appje", response.errorBody().string());
                displayNotification("Rit niet opgeslagen", "Er is iets fout gegaan bij het uploaden van de rit.");
                return Result.failure();
            }
        } catch(Exception e) {
            Log.d("appje", String.valueOf(e));
            return Result.failure();
        }
    }

    private void displayNotification(String title, String body){
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context, CHANNEL_ID)
                .setSmallIcon(R.drawable.car_80)
                .setContentTitle(title)
                .setContentText(body)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT);


        NotificationManagerCompat notificationManagerCompat = NotificationManagerCompat.from(context);
        notificationManagerCompat.notify(1, mBuilder.build());
    }
}
