package dev.meulen.kilometerapp.data.database;

import android.content.Context;

import androidx.room.Room;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import dagger.hilt.InstallIn;
import dagger.hilt.android.components.ApplicationComponent;
import dagger.hilt.android.qualifiers.ApplicationContext;
import dev.meulen.kilometerapp.data.database.dao.RitDao;
import dev.meulen.kilometerapp.data.database.dao.UserDao;

@Module
@InstallIn(ApplicationComponent.class)
public class AppDatabaseModule {
    @Provides
    @Singleton
    public static AppDatabase provideAppDatabase(@ApplicationContext Context context) {
        return Room.databaseBuilder(context, AppDatabase.class, "kilometer_database").fallbackToDestructiveMigration().build();
    }

    @Provides
    public static UserDao provideUserDao(AppDatabase database) {
        return database.userDao();
    }

    @Provides
    public static RitDao provideRitDao(AppDatabase database) {
        return database.ritDao();
    }

    @Provides
    @Singleton
    public static ExecutorService provideDatabaseWriteExecutor() {
        return Executors.newFixedThreadPool(4);
    }
}
