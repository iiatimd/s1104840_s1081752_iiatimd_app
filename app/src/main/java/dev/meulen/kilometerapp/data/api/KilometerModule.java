package dev.meulen.kilometerapp.data.api;

import com.google.gson.GsonBuilder;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import dagger.hilt.InstallIn;
import dagger.hilt.android.components.ApplicationComponent;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
@InstallIn(ApplicationComponent.class)
class KilometerModule {

    @Provides
    @Singleton
    public static KilometerService provideKilometerService() {
        return new Retrofit.Builder()
//                .baseUrl("http://192.168.1.150/api/")
                .baseUrl("https://iiatimd-api-sex6x4izeq-ez.a.run.app/api/")
                .addConverterFactory(GsonConverterFactory.create(new GsonBuilder().setLenient().create()))
                .build()
                .create(KilometerService.class);
    }

}