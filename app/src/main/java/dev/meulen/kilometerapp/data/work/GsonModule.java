package dev.meulen.kilometerapp.data.work;

import com.google.gson.Gson;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import dagger.hilt.InstallIn;
import dagger.hilt.android.components.ApplicationComponent;

@Module
@InstallIn(ApplicationComponent.class)
public class GsonModule {
    @Provides
    @Singleton
    public static Gson gson() {
        return new Gson();
    }
}
