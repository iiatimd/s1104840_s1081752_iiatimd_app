package dev.meulen.kilometerapp;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;

import javax.inject.Inject;

import dagger.hilt.android.AndroidEntryPoint;
import dev.meulen.kilometerapp.data.LoginRepository;
import dev.meulen.kilometerapp.ui.login.LoginActivity;
import dev.meulen.kilometerapp.ui.ritten.RittenOverzichtActivity;

@AndroidEntryPoint
public class MainActivity extends AppCompatActivity {

    @Inject LoginRepository loginRepository;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // Initiate Activity
        super.onCreate(savedInstanceState);
        Intent activityIntent;

        // Check if the user is already logged in with the injected LoginRepository
        if(loginRepository.isLoggedIn()) {
            activityIntent = new Intent(this, RittenOverzichtActivity.class);
        } else {
            activityIntent = new Intent(this, LoginActivity.class);
        }

        // Send the user to the right page accordingly
        startActivity(activityIntent);

        // Make sure the user can't go back to this activity
        finish();
    }

}